package com.lastnews.component.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LastNewsClient {
    private static final String BaseUrl="https://newsapi.org/";
    private static Retrofit retrofit=null;
    public static Retrofit getClient(){
        Gson gson=new GsonBuilder()
                .setLenient()
                .create();
        OkHttpClient okHttpClient=new OkHttpClient.Builder()
                .build();
        if(retrofit==null){
            retrofit=new Retrofit.Builder()
                    .baseUrl(BaseUrl)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;
    }
}
