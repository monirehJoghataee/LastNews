package com.lastnews.ui.main.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.lastnews.lastnews.R;
import com.lastnews.ui.main.model.Article;

import java.util.List;


public class BBCNewsAdapter extends RecyclerView.Adapter<BBCNewsAdapter.Viewholder> implements View.OnClickListener {

    private List<Article> modelNewsSports;
    Context ctx;
    Activity activity;

    public BBCNewsAdapter(List<Article> modelNewsSports, Activity activity) {
        this.modelNewsSports = modelNewsSports;
        this.activity=activity;
    }


    @NonNull
    @Override
    public BBCNewsAdapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.news_list_model,parent,false);
        Viewholder holder=new Viewholder(view);
        ctx=parent.getContext();
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        Glide.with(ctx).load(modelNewsSports.get(position).getUrlToImage()).into(holder.img_news);
        holder.btn_ViewNews.setOnClickListener(this);
        holder.btn_ViewNews.setTag(position);
        holder.tv_publishedAt.setText(modelNewsSports.get(position).getPublishedAt());
        holder.tv_description.setText(modelNewsSports.get(position).getDescription());
        holder.tv_title.setText(modelNewsSports.get(position).getTitle());
        holder.tv_author.setText(modelNewsSports.get(position).getAuthor());
    }

    @Override
    public int getItemCount() {
        return modelNewsSports.size();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_ViewNews:
                sendToLink(modelNewsSports.get((Integer) view.getTag()).getUrl());
                break;
        }
    }

    public class Viewholder extends RecyclerView.ViewHolder
    {
        TextView tv_author,tv_title,tv_description,tv_publishedAt;
        Button btn_ViewNews;
        ImageView img_news;
        public Viewholder(View itemView) {
            super(itemView);
            tv_author=itemView.findViewById(R.id.tv_author);
            tv_title=itemView.findViewById(R.id.tv_title);
            tv_description=itemView.findViewById(R.id.tv_description);
            tv_publishedAt=itemView.findViewById(R.id.tv_publishedAt);
            btn_ViewNews=itemView.findViewById(R.id.btn_ViewNews);
            img_news=itemView.findViewById(R.id.img_news);
        }
    }
    protected void sendToLink(String url) {

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if(activity!=null)
            activity.startActivity(i);
    }
}
