package com.lastnews.ui.base;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import com.lastnews.lastnews.R;
import com.lastnews.ui.main.MainContract;

public class BaseActivity extends AppCompatActivity {
    Dialog dialog_loading = null;
    public boolean IsNetworkConnected(){
        try {
            boolean status = false;
            ConnectivityManager ConnectionManager=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo=ConnectionManager.getActiveNetworkInfo();
            if(networkInfo == null)
            {
                status= false;
            }
            else if(!networkInfo.isAvailable())
            {
                status=false;
            }
            else if(!networkInfo.isConnected())
            {
                status=false;
            }
            else status=true;

            return status;
        }catch (Exception e){}
        return true;
    }
    public void showDialogLoading(boolean visible){
        if(visible==true) {
            dialog_loading = new Dialog(BaseActivity.this);
            dialog_loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_loading.setCancelable(false);
            dialog_loading.setContentView(R.layout.dialog_loading);
            dialog_loading.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog_loading.getWindow().getAttributes().windowAnimations = R.style.DialogTheme; //style id
            dialog_loading.show();
        }
        else dialog_loading.dismiss();
    }

    public void showNoNetDialog(final MainContract View, final String type){
        final Dialog dialog = new Dialog(BaseActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_no_net);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme; //style id

        Button settingBtn = (Button) dialog.findViewById(R.id.dialog_btn_setting);
        Button tryBtn = (Button) dialog.findViewById(R.id.dialog_btn_refresh);

        tryBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                switch (type){
                    case "sport":
                        View.getSport();
                        break;
                    case "news":
                        View.getNews();
                        break;
                }

            }

        });

        settingBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(
                        android.provider.Settings.ACTION_WIFI_SETTINGS));
            }
        });
        ImageView exitBtn = (ImageView) dialog.findViewById(R.id.dialog_img_close);
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
