package com.lastnews.ui.main;

import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.lastnews.lastnews.R;
import com.lastnews.ui.base.BaseActivity;
import com.lastnews.ui.main.adapter.BBCNewsAdapter;
import com.lastnews.ui.main.model.Article;
import com.novoda.merlin.Merlin;
import com.novoda.merlin.registerable.connection.Connectable;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.NewsRecycle)
    RecyclerView NewsRecycler;
    @BindView(R.id.BtnType)
    FloatingActionButton BtnNewsType;
    @BindView(R.id.tv_NoData)
    TextView TvNoData;
    MainPresenter mainPresenter;

    private boolean isSport = false;
    Merlin merlin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Initialize();
    }

    private void Initialize() {
        ButterKnife.bind(this);
        mainPresenter = new MainPresenter(this, this);
        merlin = new Merlin.Builder().withConnectableCallbacks().build(this);
        merlin.registerConnectable(new Connectable() {
            @Override
            public void onConnect() {
                Toast.makeText(getBaseContext(), "Internet is Available", Toast.LENGTH_LONG).show();
                mainPresenter.getNews();
            }
        });


    }


    public void setRecycelerView(ArrayList<Article> Article) {
        NewsRecycler.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        NewsRecycler.setHasFixedSize(true);
        NewsRecycler.setAdapter(new BBCNewsAdapter(Article, this));
    }


    public void NoData(boolean status) {
        if (status) {
            TvNoData.setVisibility(View.VISIBLE);
            NewsRecycler.setVisibility(View.GONE);
        } else {
            TvNoData.setVisibility(View.GONE);
            NewsRecycler.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.BtnType)
    public void SelectNewsType() {
        if (isSport) {
            ClickgetNews();
        } else {
            ClickgetSport();
        }
    }

    private void ClickgetSport() {
        BtnNewsType.setImageResource(R.drawable.ic_news_white);
        isSport = false;
        mainPresenter.getSport();
    }

    private void ClickgetNews() {
        BtnNewsType.setImageResource(R.drawable.ic_sport_white);
        isSport = true;
        mainPresenter.getNews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        merlin.bind();
    }

    @Override
    protected void onPause() {
        super.onPause();
        merlin.unbind();
    }

}
