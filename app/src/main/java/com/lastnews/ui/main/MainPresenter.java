package com.lastnews.ui.main;

import android.app.Activity;

import com.lastnews.component.network.LastNewsClient;
import com.lastnews.component.network.LastNewsInterface;
import com.lastnews.ui.main.model.NewsModel;
import com.lastnews.ui.main.model.Article;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenter implements MainContract{
    private Activity context;
    private MainActivity view;

    MainPresenter(Activity context, MainActivity view) {
        this.context = context;
        this.view = view;
    }

    @Override
    public void getNews() {
        if (view.IsNetworkConnected()) {
            view.showDialogLoading(true);
            LastNewsInterface lastNewsInterface = LastNewsClient.getClient().create(LastNewsInterface.class);
            Call<NewsModel> call = lastNewsInterface.getBbcNews();
            call.enqueue(new Callback<NewsModel>() {
                @Override
                public void onResponse(Call<NewsModel> call, Response<NewsModel> response) {
                    view.showDialogLoading(false);
                    if (response.isSuccessful()) {
                        if (response.body().getStatus().equals("ok")) {
                            ArrayList<Article> entries = new ArrayList<>();
                            for (int i = 0; i < response.body().getArticles().size(); i++) {
                                Article model = new Article();
                                model.setAuthor(response.body().getArticles().get(i).getAuthor());
                                model.setDescription(response.body().getArticles().get(i).getDescription());
                                model.setPublishedAt(response.body().getArticles().get(i).getPublishedAt());
                                model.setTitle(response.body().getArticles().get(i).getTitle());
                                model.setUrl(response.body().getArticles().get(i).getUrl());
                                model.setUrlToImage(response.body().getArticles().get(i).getUrlToImage());
                                entries.add(model);
                            }
                            if (entries.size() > 0) {
                                view.setRecycelerView(entries);
                                view.NoData(false);
                            } else view.NoData(true);
                        }
                    }

                }

                @Override
                public void onFailure(Call<NewsModel> call, Throwable t) {
                    view.showDialogLoading(false);
                }
            });
        } else view.showNoNetDialog(this, "news");
    }

    @Override
    public void getSport() {
        if (view.IsNetworkConnected()) {
            view.showDialogLoading(true);
            LastNewsInterface lastNewsInterface = LastNewsClient.getClient().create(LastNewsInterface.class);
            Call<NewsModel> call = lastNewsInterface.getBbcSport();
            call.enqueue(new Callback<NewsModel>() {
                @Override
                public void onResponse(Call<NewsModel> call, Response<NewsModel> response) {
                    view.showDialogLoading(false);
                    if (response.body().getStatus().equals("ok")) {
                        ArrayList<Article> entries = new ArrayList<>();
                        for (int i = 0; i < response.body().getArticles().size(); i++) {
                            Article model = new Article();
                            model.setAuthor(response.body().getArticles().get(i).getAuthor());
                            model.setDescription(response.body().getArticles().get(i).getDescription());
                            model.setPublishedAt(response.body().getArticles().get(i).getPublishedAt());
                            model.setTitle(response.body().getArticles().get(i).getTitle());
                            model.setUrl(response.body().getArticles().get(i).getUrl());
                            model.setUrlToImage(response.body().getArticles().get(i).getUrlToImage());
                            entries.add(model);
                        }
                        if (entries.size() > 0) {
                            view.setRecycelerView(entries);
                            view.NoData(false);
                        } else view.NoData(true);
                    }
                }

                @Override
                public void onFailure(Call<NewsModel> call, Throwable t) {
                    view.showDialogLoading(false);
                }
            });
        } else view.showNoNetDialog(this, "sport");
    }
}
