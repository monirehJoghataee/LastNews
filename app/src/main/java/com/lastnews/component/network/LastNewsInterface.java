package com.lastnews.component.network;

import com.lastnews.ui.main.model.NewsModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface LastNewsInterface {
    @GET("v1/articles?source=bbc-news&sortBy=top&apiKey=d2f218bf8c4e4e4892c1a6feb2102ac1")
    Call<NewsModel> getBbcNews();
    @GET("v1/articles?source=bbc-sport&sortBy=top&apiKey=d2f218bf8c4e4e4892c1a6feb2102ac1")
    Call<NewsModel> getBbcSport();
}
